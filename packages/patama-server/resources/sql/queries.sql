-- :name create-user! :! :n
-- :doc creates a new user record
INSERT INTO users
(username, password_hash)
VALUES (:username, :password_hash)

-- :name get-user :? :1
-- :doc gets user with given username from db
SELECT username, password_hash FROM users
WHERE username = :username

-- :name check-user-existence :? :1
-- :doc checks if user with given username is already in db
SELECT username FROM users
WHERE username = :username

-- :name create-user-session! :<!
-- :doc creates new user session
INSERT INTO user_sessions
(username, hash)
VALUES (:username, :hash)

-- :name check-user-session-existence :? :1
SELECT username, hash FROM user_sessions
WHERE hash = :hash

-- :name get-user-information :? :1
SELECT u.username, u.full_name, u.email, u.occupation, u.is_admin
FROM users AS u
    LEFT JOIN user_sessions AS us ON u.username = us.username
    WHERE us.hash = :session_token

-- :name get-username-list :? :*
SELECT username FROM users


-- :name create-task! :<!
-- :doc creates a new task record
INSERT INTO tasks
(title, description, until, created_by, status, board_id)
VALUES (:title, :description, :until, :created_by, :status, :board_id) RETURNING id

-- :name update-task-status! :! :n
-- :doc updates an existing task's status
UPDATE tasks
SET status = :status
WHERE id = :id

-- :name update-task-until! :! :n
-- :doc sets or updates an existing task's deadline
UPDATE tasks
SET until = :until
WHERE id = :id

-- :name get-tasks :? :n
-- :doc retrieves all tasks in database (not recommended!!)
SELECT * FROM tasks

-- :name get-task :? :1
-- :doc retrieves a task record given the id
SELECT * FROM tasks
WHERE id = :id

-- :name get-tasks-by-status :? :*
-- :doc retrieves tasks with given status
SELECT * FROM tasks
WHERE status = :status AND board_id = :board_id


-- :name update-task! :! :n
UPDATE tasks
SET
id = id
--~ (when (contains? params :title) ", title = :title")
--~ (when (contains? params :description) ", description = :description")
--~ (when (contains? params :status) ", status = :status")
--~ (when (contains? params :date) ", until = :date")
WHERE id = :id

-- :name delete-task! :! :n
-- :doc deletes a task given the id
DELETE FROM tasks
WHERE id = :id

-- :name set-task-responsible! :! :n
-- :doc creates a relation between task & user
INSERT INTO tasks_responsibles
(task_id, user_id)
VALUES (:task_id, :user_id)

-- :name get-task-responsibles :? :*
SELECT user_id AS username FROM tasks_responsibles
WHERE task_id = :task_id

-- :name get-task-tags :? :*
SELECT t.id, t.label, t.color FROM tags AS t
	LEFT JOIN tasks_tags AS tt ON t.id = tt.tag_id
	WHERE task_id = :task_id

-- :name set-task-responsibles! :<!
INSERT INTO tasks_responsibles
(task_id, user_id)
VALUES :t*:responsibles 

-- :name delete-task-responsibles! :! :n
DELETE FROM tasks_responsibles
WHERE task_id = :task_id
AND user_id IN (:v*:responsibles)

-- :name set-task-tags! :<!
INSERT INTO tasks_tags
(task_id, tag_id)
VALUES :t*:tags

-- :name delete-task-tags! :! :n
DELETE FROM tasks_tags
WHERE task_id = :task_id
AND tag_id IN (:v*:tags)

-- tasks

-- :name get-available-tags :? :*
-- :doc retrieves available tags
SELECT * FROM tags

-- :name get-specific-tag :? :1
SELECT * FROM tags
WHERE id = :id

-- :name get-multiple-tags-by-ids :? :n
SELECT * FROM tags
WHERE id IN (:i*:ids)

-- :name get-tags-by-board :? :*
SELECT id, color, label FROM tags
WHERE board_id = :board_id


-- :name create-tag! :<!
-- :doc create new tag record
INSERT INTO tags
(id, color, label)
VALUES (:id, :color, :label)

-- :name create-tags! :<!
INSERT INTO tags
(id, color, label, board_id)
VALUES :t*:tags

-- :name update-tag! :! :n
-- :doc update tag color or/and label
UPDATE tags
SET
id = id
--~ (when (contains? params :color) ", color = :color")
--~ (when (contains? params :label) ", label = :label")
WHERE id = :id

-- :name update-tag-color! :! :n
-- :doc edit tag color
UPDATE tags
SET color = :color
WHERE id = :id

-- :name update-tag-label! :! :n
-- :doc edit tag label
UPDATE tags
SET label = :label
WHERE id = :id

-- :name delete-tag! :! :n
UPDATE tags
SET board_id = NULL
WHERE id = :id

-- :name get-board-tags :? :*
SELECT
(id, color, label)
FROM tags
WHERE board_id = :board_id


-- :name get-statuses :? :*
SELECT * FROM statuses
WHERE board_id = :board_id

-- :name create-status! :! :<!
INSERT INTO statuses
(id, color, label)
VALUES (:id, :color, :label)

-- :name create-statuses! :<!
INSERT INTO statuses
(id, color, label, board_id)
VALUES :t*:statuses

-- :name update-status! :<!
UPDATE statuses
SET
id = id
--~ (when (contains? params :color) ", color = :color")
--~ (when (contains? params :label) ", label = :label")
WHERE id = :id

-- :name delete-status! :! :n
UPDATE statuses
SET board_id = NULL
WHERE id = :id

-- :name get-board-statuses :? :*
SELECT
(id, color, label)
FROM statuses
WHERE board_id = :board_id


-- ATTACHING

-- :name tag-task! :! :n
INSERT INTO tasks_tags
(task_id, tag_id)
VALUES :tuple*:task_tags


-- DETACHING

-- :name detach-task-from-tags! :! :n
DELETE FROM tasks_tags WHERE task_id = :task_id

-- :name detach-task-from-responsibles! :! :n
DELETE FROM tasks_responsibles WHERE task_id = :task_id

-- :name detach-tag! :! :n
DELETE FROM tasks_tags WHERE tag_id = :tag_id


-- BOARDS

-- :name get-board-by-id :? :1
SELECT * FROM boards WHERE id = :id;

-- :name get-boards :? :*
SELECT * FROM boards WHERE is_deleted = false;

-- :name get-board-by-title :? :1
SELECT * FROM boards
WHERE title = :title AND is_deleted = false;

-- :name get-boards-by-project :? :*
SELECT id, title FROM boards
WHERE project_id = :project_id AND is_deleted = false;

-- :name create-board! :<!
INSERT INTO boards
(title
--~ (when (contains? params :project_id) ", project_id")
)
VALUES (:title
--~ (when (contains? params :project_id) ", :project_id")
)
RETURNING id

-- :name update-board! :! :n
UPDATE boards
SET
id = id
--~ (when (contains? params :title) ", title = :title")
WHERE id = :id

-- :name delete-board! :! :n
UPDATE boards SET is_deleted = true WHERE id = :id


-- PROJECTS

-- :name create-project! :<!
INSERT INTO projects
(title, owner)
VALUES (:title, :owner)
RETURNING id

-- :name get-project-by-id :? :1
SELECT id, title, owner, beginning, deadline
FROM projects
WHERE id = :id

-- :name get-owned-projects :? :*
SELECT
p.id, p.title, p.owner, b.id AS board_id
FROM projects p
    LEFT JOIN LATERAL (
        SELECT * FROM boards b
        WHERE p.id = b.project_id
        LIMIT 1
    ) b ON TRUE
    LEFT JOIN project_participants AS pp ON p.id = pp.project_id
WHERE p.owner = :owner OR pp.participant = :owner

-- :name get-owned-project-ids :? :*
SELECT id FROM projects WHERE owner = :owner

-- :name get-participated-projects :? :*
SELECT p.id, p.title
FROM project_participants AS pp
    LEFT JOIN projects AS p ON p.id = pp.project_id
    WHERE pp.participant = :participant

-- :name get-project-participants :? :*
SELECT participant
FROM project_participants
WHERE project_id = :project_id

-- :name add-project-participants! :<!
INSERT INTO project_participants
(project_id, participant)
VALUES :tuple*:participants

-- :name delete-project-participants! :! :n
DELETE FROM project_participants
WHERE project_id = :project_id
AND participant IN (:v*:participants) 

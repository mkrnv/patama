CREATE TABLE user_sessions
(
    id SERIAL,
    username VARCHAR(128),
    hash VARCHAR(256),
    timestamp TIMESTAMP default current_timestamp,

    PRIMARY KEY(id),
    CONSTRAINT fk_user
        FOREIGN KEY(username) 
            REFERENCES users(username)
);
--;;
ALTER TABLE users ADD COLUMN password_hash VARCHAR(256);

ALTER TABLE statuses
ADD COLUMN board_id INTEGER;
--;;
ALTER TABLE statuses ADD CONSTRAINT fk_status_board
    FOREIGN KEY(board_id)
        REFERENCES boards(id);
--;;
ALTER TABLE tags
ADD COLUMN board_id INTEGER;
--;;
ALTER TABLE tags ADD CONSTRAINT fk_tag_board
    FOREIGN KEY(board_id)
        REFERENCES boards(id);

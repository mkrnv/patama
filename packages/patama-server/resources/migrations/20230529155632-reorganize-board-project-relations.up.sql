ALTER TABLE boards ADD COLUMN project_id INTEGER;
--;;
ALTER TABLE boards
ADD CONSTRAINT fk_project_board
    FOREIGN KEY(project_id)
        REFERENCES projects(id);


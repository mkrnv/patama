CREATE TABLE projects
(
    id SERIAL,
    title TEXT,
    description TEXT,
    beginning TIMESTAMP,
    deadline TIMESTAMP,
    owner VARCHAR(128),

    PRIMARY KEY(id),
    CONSTRAINT fk_project_owner
        FOREIGN KEY(owner)
            REFERENCES users(username)
);
--;;
CREATE TABLE project_participants
(
    id SERIAL,
    project_id INTEGER,
    participant VARCHAR(128),

    PRIMARY KEY(id),
    CONSTRAINT fk_project_participants
        FOREIGN KEY(participant)
            REFERENCES users(username),
    CONSTRAINT fk_participated_project
        FOREIGN KEY(project_id)
            REFERENCES projects(id)
);

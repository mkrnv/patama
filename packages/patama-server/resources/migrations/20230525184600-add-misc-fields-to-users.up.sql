ALTER TABLE users
ADD COLUMN is_admin BOOLEAN
    NOT NULL DEFAULT false;
--;;
ALTER TABLE users
ADD COLUMN full_name TEXT;
--;;
ALTER TABLE users
ADD COLUMN occupation TEXT;
--;;
ALTER TABLE users
ADD COLUMN email TEXT;

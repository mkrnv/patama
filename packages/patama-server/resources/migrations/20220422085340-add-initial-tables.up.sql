CREATE TABLE users
(
    username VARCHAR(128),
    created TIMESTAMP default current_timestamp,

    PRIMARY KEY(username)
);
--;;
CREATE TABLE statuses
(
    id VARCHAR(64),
    label VARCHAR(256),

    PRIMARY KEY(id)
);
--;;
CREATE TABLE tags
(
    id VARCHAR(64),
    label VARCHAR(256),
    color VARCHAR(256),

    PRIMARY KEY(id)
);
--;;
CREATE TABLE tasks
(
    id SERIAL,
    title VARCHAR(256),
    description TEXT,
    timestamp TIMESTAMP default current_timestamp,
    until TIMESTAMP,
    created_by VARCHAR(128),
    status VARCHAR(64),

    PRIMARY KEY(id),
    CONSTRAINT fk_user
        FOREIGN KEY(created_by)
            REFERENCES users(username),
    CONSTRAINT fk_status
        FOREIGN KEY(status)
            REFERENCES statuses(id)
);
--;;
CREATE TABLE tasks_responsibles
(
    task_id INT,
    user_id VARCHAR(128),

    CONSTRAINT fk_task
        FOREIGN KEY(task_id)
            REFERENCES tasks(id),
    CONSTRAINT fk_user
        FOREIGN KEY(user_id)
            REFERENCES users(username)
);
--;;
CREATE TABLE tasks_tags
(
    task_id INT,
    tag_id VARCHAR(64),

    CONSTRAINT fk_task
        FOREIGN KEY(task_id)
            REFERENCES tasks(id),
    CONSTRAINT fk_tag
        FOREIGN KEY(tag_id)
            REFERENCES tags(id)
);

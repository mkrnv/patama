CREATE TABLE boards
(
    id SERIAL,
    title TEXT,
    timestamp TIMESTAMP default current_timestamp,

    PRIMARY KEY(id)
);
--;;
ALTER TABLE tasks ADD COLUMN board_id INT;
--;;
ALTER TABLE tasks ADD CONSTRAINT fk_task_board
    FOREIGN KEY(board_id)
        REFERENCES boards(id);
--;;
INSERT INTO boards(title) VALUES ('Default Board') RETURNING id;
--;;
UPDATE tasks
SET board_id =
(
    SELECT boards.id
    FROM boards
    WHERE boards.title = 'Default Board'
);

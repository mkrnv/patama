(ns patama-server.services.projects
  (:require
    [patama-server.db.core :as db]
    [clojure.core :as c]
    [clojure.set :as s]))

(def not-allowed-to-view-project "not allowed to view project")

(defn create-project!
  [params]
  (let [title (get params :title)
        owner (get params :owner)
        project-create-result (db/create-project!
                                {:title title
                                 :owner owner})
        new-project-id        (when
                                (vector? project-create-result)
                                (:id (first project-create-result)))
        board-creation-result (db/create-board! {:title title
                                                 :project_id new-project-id})
        ;new-board-id          (when
        ;                        (vector? board-creation-result)
        ;                        (get
        ;                          (first board-creation-result) :id))
        ]
    new-project-id))

(defn get-by-id
  [id request-username]
  (let [project-id                  (Integer/parseInt id)
        user-owned-projects         (map
                                      (fn [project] (:id project))
                                      (db/get-owned-project-ids {:owner request-username}))
        user-participated-projects  (map
                                      (fn [participation] (:project_id participation))
                                      (db/get-participated-projects {:participant request-username}))
        permissions                 (vec (concat user-owned-projects user-participated-projects))
        allowed-to-view-project     (.contains permissions project-id)
        project                     (when allowed-to-view-project
                                      (db/get-project-by-id {:id project-id}))
        project-with-team           (when project
                                      (assoc project
                                        :participants (map #(:participant %)
                                                           (db/get-project-participants {:project_id project-id}))))
        result                      (if allowed-to-view-project
                                      project-with-team
                                      not-allowed-to-view-project)]
    result))


(defn get-owned
  [owner]
  (let [projects (db/get-owned-projects {:owner owner})
        unique-ids (-> (set (map #(:id %) projects))
                       seq)
        unique-projects (map
                          (fn [p-id] (-> (filter #(= (:id %) p-id) projects)
                                         first))
                          unique-ids)]
    unique-projects))

(defn get-participated
  [participant]
  (db/get-participated-projects {:participant participant}))

(defn get-project-participants
  [params]
  (let [project-id (Integer/parseInt (:project_id params))]
    (map #(:participant %) (db/get-project-participants {:project_id project-id}))))

(defn update-participants!
  [params]
  (let [project-id    (:project_id params)
        participants  (:participants params)
        real-members  (map #(:participant %) (db/get-project-participants {:project_id project-id}))
        new-members   (-> (s/difference
                            (set participants)
                            (set real-members))
                          seq)
        left-members  (-> (s/difference
                            (set real-members)
                            (set participants))
                          seq)]
    (when (> (count new-members) 0)
      (db/add-project-participants! {:participants (map (fn [p] [project-id p]) new-members)}))
    (when (> (count left-members) 0)
      (db/delete-project-participants! {:project_id project-id
                                        :participants left-members}))))

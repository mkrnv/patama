(ns patama-server.services.tags
  (:require 
    [patama-server.db.core :as db]))

(defn check-task-tags
  [task-id]
  (let [task-tag-ids  (map
                        (fn [tag] (get tag :tag-id))
                        (db/get-task-tags
                          {:task_id task-id}))]
    (db/get-multiple-tags-by-ids {:ids task-tag-ids})))

(defn tag-task
  [tag-id task-id]
  (let [result (db/tag-task!
                 {:task_id task-id
                  :tag_id tag-id})]
    result))

(defn get-tags-by-board
  [params]
  (let [id (:id params)]
    (println (type id))
    (db/get-tags-by-board {:board_id id})))

(defn get-available-tags
  [params]
  (db/get-available-tags {:board_id (:board_id params)}))

(defn create-tag
  [params]
  (let [id          (get params :id)
        tag-exists  (some? (db/get-specific-tag {:id id}))]
    (when-not (and (nil? id) (not tag-exists))
      (let [color-param     (get params :color)
            color           (if
                              (nil? color-param)
                                "bg-slate-300"
                                color-param)
            label-param     (get params :label)
            label           (if
                              (nil? label-param)
                                id
                                label-param)
            creation-result (db/create-tag! {:id id
                                             :color color
                                             :label label})]
        (= creation-result 1)))))

(defn update-tag
  [params]
  (let [id (get params :id)
        no-id-given (nil? id)
        tag (db/get-specific-tag {:id id})]
    (when (not no-id-given)
      (let [color         (get params :color)
            label         (get params :label)
            update-result (db/update-tag!
                            {:id id
                             :color (if
                                      (some? color)
                                        color
                                        (get tag :color))
                             :label (if
                                      (some? label)
                                        label
                                        (get tag :label))})]
        (= update-result 1)))))

(ns patama-server.services.boards
  (:require
    [clojure.set :as s]
    [patama-server.db.core :as db]
    [patama-server.services.tasks :as tasks]
    [patama-server.services.tags :as tags]))

(def no-title-param "no \"title\" parameter was given")
(def board-already-exists "board with this title already exists")
(def board-doesnt-exist "board does not exist")
(def board-delete-success "board deleted successfully")
(def no-board-id "board id was not provided")
(def incorrect-board-id "incorrect board id")

(defn -render-boards
  [boards]
  (map
    (fn [board] (dissoc board :is_deleted :timestamp))
    boards))

(defn get-boards
  []
  (let [boards (db/get-boards)
        rendered-boards (-render-boards boards)]
    rendered-boards))

(defn get-boards-by-params
  [params]
  (let [id (get params :id)
        project-id (get params :project_id)
        result (if (some? id)
                 (let [board    (db/get-board-by-id {:id (Integer/parseInt id)})
                       columns  (tasks/get-tasks-by-statuses {:board_id id})
                       tags     (tags/get-tags-by-board {:id (Integer/parseInt id)})]
                 {:id id
                  :project_id (:project_id board)
                  :title (:title board)
                  :columns columns
                  :tags tags})
                 (if (some? project-id)
                   (db/get-boards-by-project {:project_id (Integer/parseInt project-id)})))]
    result))

(defn create-board!
  [params]
  (let [title             (get params :title)
        no-title          (nil? title)
        board-exists      (some? (db/get-board-by-title {:title title}))
        creation-criteria (and (not no-title) (not board-exists))
        creation-result   (when creation-criteria
                            (db/create-board! {:title title}))
        new-board-id      (when
                            (vector? creation-result)
                            (get (first creation-result) :id))]
    (if (int? new-board-id)
      new-board-id
      (map first
      (filter
        (fn [c] (true? (last c)))
        [[no-title-param no-title]
         [board-already-exists board-exists]])))))

(defn delete-board!
  [params]
  (let [id                (try 
                            (Integer/parseInt (:id params))
                            (catch NumberFormatException _ "incorrect id"))
        no-id             (nil? id)
        correct-id        (if (not no-id)
                            (int? id)
                            no-id)
        deletion-criteria (and (not no-id) correct-id)
        deletion-result   (when deletion-criteria
                            (db/delete-board! {:id id}))
        board-deleted     (and
                            (int? deletion-result)
                            (= deletion-result 1))
        board-not-exist   (if (not board-deleted)
                            (= deletion-result 0)
                            false)]
    (if (and deletion-criteria board-deleted)
      board-delete-success
      (map first
      (filter
        (fn [c] (true? (last c)))
        [[no-board-id no-id]
         [incorrect-board-id (not correct-id)]
         [board-doesnt-exist board-not-exist]])))))

(defn -check-column-for-changes
  [columns current-column]
  (let [changed   current-column
        original  (-> (filter #(= (:id %) (:id current-column)) columns)
                      first)
        new-col   (nil? original)
        changes   (if new-col
                    changed
                    (filter some? (map
                      (fn [column-key]
                        (when
                          (or
                            (not=
                              (get changed column-key)
                              (get original column-key))
                            (= column-key :id))
                          [column-key (get changed column-key)]))
                      (keys changed))))]
    changes))

(defn -detect-updates!
  [board-id statuses tags change]
  (let [change-type   (first change)
        change-value  (last change)
        board         (db/get-board-by-id {:id board-id})]
    (case change-type
      :title
        (when (not= (:title board) change-value)
          [change-type change-value])
      :columns
        (let [changed-columns (map (partial -check-column-for-changes statuses)
                                change-value)]
          [change-type changed-columns])
      :tags
        (let [changed-tags    (map (partial -check-column-for-changes tags)
                                change-value)]
          [change-type changed-tags])
      (do ))))

(defn update-board!
  [params]
  (when
    (some? (:id params))
    (let [id                (Integer/parseInt (:id params))
          statuses          (db/get-statuses {:board_id id})
          tags              (db/get-tags-by-board {:board_id id})
          changes           (dissoc params :id)

          tag-ids           (map #(:id %) (:tags params))
          real-tag-ids      (map #(:id %) tags)
          new-tag-ids       (-> (s/difference (set tag-ids) (set real-tag-ids))
                                seq)
          deleted-tag-ids   (-> (s/difference (set real-tag-ids) (set tag-ids))
                                seq)
          detected-updates  (-> (map
                                  (partial -detect-updates! id statuses tags)
                                  (seq changes)))
          result-map        (into {} detected-updates)
          basic-updates     (-> (dissoc result-map :columns)
                                (dissoc :tags))
          basic-update-res  (db/update-board! (assoc basic-updates :id id))

          transform-tuple   (fn [u] [(:id u) (:color u) (:label u) id])

          column-updates    (:columns result-map)
          new-columns       (filter
                              (fn [col]
                                (-> (some
                                      #(= (:id col) (:id %))
                                      (map #(:id %) statuses))
                                    not))
                              column-updates)
          old-columns       (filter
                              (fn [col]
                                (some
                                  #(= (:id col) (:id %))
                                  (map #(:id %) statuses)))
                              column-updates)
          new-col-result    (when
                              (> (count new-columns) 0)
                              (db/create-statuses! {:statuses (map transform-tuple new-columns)}))

          tag-updates       (:tags result-map)
          new-tags          (filter #(some (fn [tag-id] (= tag-id (:id %))) new-tag-ids) (:tags params))
          old-tags          (filter
                              (fn [col]
                                (some
                                  #(= (:id col) (:id %))
                                  (map #(:id %) tags)))
                              tag-updates)
          new-tag-result    (when
                              (> (count new-tags) 0)
                              (db/create-tags! {:tags (map transform-tuple new-tags)}))
          
          columns-to-delete (seq (s/difference
                              (set (map #(:id %) statuses))
                              (set (map #(:id %) (:columns params)))))
          tags-to-delete    (seq (s/difference
                                (set (map #(:id %) tags))
                                (set (map #(:id %) (:tags params)))))]
      (do
        (println new-tags)
        (when
          (> (count old-columns) 0)
          (doseq [s old-columns]
            (when (> (count s) 0)
              (db/update-status! (into {} s)))))
        (when
          (> (count old-tags) 0)
          (doseq [t old-tags]
            (when (> (count t) 0)
              (db/update-tag! (into {} t)))))
        (when 
          (> (count columns-to-delete) 0)
          (doseq [s columns-to-delete]
            (db/delete-status! {:id s})))
        (when 
          (> (count tags-to-delete) 0)
          (doseq [t tags-to-delete]
            (db/delete-tag! {:id t})))))))

(ns patama-server.services.settings
  (:require
    [patama-server.db.core :as db]))

(defn get-board-settings
  [board-id]
  (let [board-statuses (db/get-board-statuses)
        board-tags (db/get-board-tags)]))

(ns patama-server.services.auth
  (:require
    [patama-server.db.core :as db]
    [buddy.core.hash :as hashing]
    [buddy.core.codecs :refer [bytes->hex]]
    [buddy.hashers :as hashers]))

(def account-created "account created successfully")
(def invalid-credentials "invalid credentials provided")
(def user-already-exists "user already exists")

(defn get-user-information
  [request]
  (let [session-token     (get-in request [:headers "authorization"])
        user-information  (when (not (nil? session-token))
                            (db/get-user-information {:session_token session-token}))]
    user-information))

(defn create-account
  [username password]
  (let [valid-username      (not-empty username)
        valid-password      (not-empty password)
        existing-user       (if
                              valid-username
                                (db/check-user-existence {:username username})
                                nil)
        not-existing-user   (nil? existing-user)
        password-hash       (hashers/derive password)
        creation-result     (if
                              (and not-existing-user valid-username valid-password) 
                                (db/create-user!
                                   {:username username
                                    :password_hash password-hash})
                                false)]
    (if (and (int? creation-result) (= creation-result 1))
      account-created
      (map first
       (filter
        (fn [c] (true? (last c)))
        [[invalid-credentials (or (not valid-username) (not valid-password))]
         [user-already-exists (not not-existing-user)]])))))


(defn- check-valid-credentials
  [username password]
  (and
    (string? username) (not-empty username)
    (string? password) (not-empty password)))

(defn create-user-session
  [username password]
  (let [valid-credentials (check-valid-credentials username password)
        user-query        (if valid-credentials
                            (db/get-user {:username username})
                            nil)
        valid-password    (if (some? user-query)
                            (hashers/check password (get user-query :password_hash))
                            false)
        session-token     (if valid-password
                            (->
                              (hashing/sha256 (str username ":" password ":" (java.time.LocalDateTime/now)))
                              (bytes->hex))
                            "")
        creation-result   (when (not-empty session-token)
                            (db/create-user-session! {:username username
                                                      :hash session-token}))]
    (when (some? creation-result) session-token)))


(defn validate-user-session
  [request]
  (let [session-token (get-in request [:headers "authorization"])]
  (and (not-empty session-token) (not-empty (db/check-user-session-existence {:hash session-token})))))

(defn get-user-list
  []
  (map #(:username %) (db/get-username-list)))

(ns patama-server.services.tasks
  (:require
    [patama-server.db.core :as db]
    [clojure.core :as c]
    [clojure.set :as s]))

(def task-created "task created successfully")
(def responsible-set "responsible set")
(def no-title-given "task title")
(def invalid-title "task title was not valid")

(defn attach-task-tags
  [task]
  (let [task-tags (db/get-task-tags {:task_id (get task :id)})]
    (assoc task :tags task-tags)))

(defn attach-task-responsibles
  [task]
  (let [responsibles (db/get-task-responsibles {:task_id (:id task)})]
    (assoc task :responsibles (map #(:username %) responsibles))))

(defn attach-task-statuses
  [status task]
  (assoc task :status status))

(defn get-task-by-id
  [id]
  (let [task (db/get-task {:id id})
        task-with-added-responsibles (assoc task
                                        :responsibles (map
                                                        #(:username %)
                                                        (db/get-task-responsibles {:task_id id})))
        task-with-added-tags (assoc task-with-added-responsibles
                                    :tags (db/get-task-tags {:task_id id}))]
    task-with-added-tags))

(defn get-tasks-by-statuses
  [params]
  (let [board-id (Integer/parseInt (get params :board_id))
        statuses (db/get-statuses {:board_id board-id})
        tasks-sorted (map
                       (fn [status]
                         (let [status-id (get status :id)
                               tasks (db/get-tasks-by-status {:status status-id
                                                              :board_id board-id})
                               tasks-with-tags (map attach-task-tags tasks)
                               tasks-with-pretty-status (map (partial attach-task-statuses status) tasks-with-tags)
                               tasks-with-responsibles (map attach-task-responsibles tasks-with-pretty-status)]
                           (-> status
                               (assoc :tasks tasks-with-responsibles)
                               (dissoc :board_id))
                            ))
                       statuses)]
    tasks-sorted))

(defn get-tasks-by
  [params]
  (let [id-param (get params :id)]
    (if (nil? id-param)
      (get-tasks-by-statuses params)
      (get-task-by-id (Integer/parseInt id-param)))))

(defn rollback-task-with-tags!
  [task-id]
  (db/detach-task! {:task_id task-id})
  (db/delete-task! {:id task-id}))

(defn create-task!
  [params]
  (let [title       (:title params)
        description (:description params)
        until       (:until params)
        created-by  (:user params)
        status      (get params :status "in-progress")
        board-id    (get params :board_id)
        no-title    (nil? title)
        valid-title (string? title)
        valid-board (number? board-id)
        ready-task  (and valid-title valid-board)
        new-task-id (when
                      ready-task
                      (get
                        (last
                          (db/create-task! 
                            {:title title
                             :description description
                             :until (java.sql.Timestamp/valueOf until)
                             :created_by created-by
                             :board_id board-id
                             :status status}))
                        :id))
        task-created (and (some? new-task-id) (int? new-task-id))

        ;; tagging
        tag-tuples      (when task-created
                          (map
                            (fn [tag-id] [new-task-id tag-id])
                            (:tags params)))
        tagging-result  (when (some? tag-tuples)
                          (if (empty? tag-tuples)
                            0
                            (db/tag-task! {:task_tags tag-tuples})))
        all-tagged      (and
                          (some? tagging-result)
                          (int? tagging-result)
                          (= (c/count tag-tuples) tagging-result))]
    (if (and task-created all-tagged)
      new-task-id
      (if (and task-created (not all-tagged))
        (rollback-task-with-tags! new-task-id)
        (map first
         (filter
          (fn [c] (true? (last c)))
          [[no-title-given no-title]
           [invalid-title (and (not no-title) (not valid-title))]]))))))

(defn- set-task-responsible!
  [task-id user-id]
  (let [result  (db/set-task-responsible! {:task_id task-id
                                           :user_id user-id})]
    (if (= result 1)
      responsible-set
      nil)))

(defn perform-updates!
  [id pair]
  (let [prop  (first pair)
        value (last pair)]
    (case prop
      :responsible
        (if (= (set-task-responsible! id value) responsible-set)
          {prop true}
          {prop false :reason "unknown"})
      :status
        (try
          (if 
            (db/update-task-status! {:id id
                                     :status value})
            {prop true}
            {prop false :reason "unknown"})
          (catch Exception _ {prop false :reason "invalid status"}))
        {prop true :note "not modified"})))

(comment 
(defn update-task!
  [params]
  (let [id (get params :id)
        to-update (dissoc params :id)
        result (map (partial perform-updates! id) (seq to-update))]
    result)))

(def allowed-updates [:id :title :description :date :status])

(defn -format-param
  [name value]
  (case name
    :date (java.sql.Timestamp/valueOf value)
    value))

(defn update-task!
  [params]
  (println params)
  (let [id          (get params :id)
        valid-id    (and (number? id) (nil? id))
        original    (when valid-id (db/get-task {:id id}))
        
        to-update-param-names   (filter
                                  (fn [param] (c/contains? params param))
                                  allowed-updates)
        to-update-param-values  (map
                                  (fn [param] (-format-param param (get params param)))
                                  to-update-param-names)
        ;; TODO: resps
        tags        (get params :tags)
        task-tags   (map
                      (fn [task-tag] (:tag_id task-tag))
                      (db/get-task-tags {:task_id id}))
        to-update   (zipmap to-update-param-names to-update-param-values)

        update-result (db/update-task! to-update)
        
        responsibles  (:responsibles params)
        real-members  (map #(:username %) (db/get-task-responsibles {:task_id id}))
        new-members   (-> (s/difference (set responsibles) (set real-members))
                          seq)
        deleted-members
                      (if (some? responsibles)
                        (-> (s/difference (set real-members) (set responsibles))
                            seq)
                        (seq []))
                      
        tags          (:tags params)
        real-tags     (map #(:id %) (db/get-task-tags {:task_id id}))
        new-tags      (-> (s/difference (set tags) (set real-tags))
                          seq)
        deleted-tags  (if (some? tags)
                        (-> (s/difference (set real-tags) (set tags))
                            seq)
                        (seq []))]
    (println new-tags deleted-tags)
    (when (> (count new-members) 0)
      (db/set-task-responsibles! {:responsibles (map
                                                  (fn [m] [id m])
                                                  new-members)}))
    (when (> (count deleted-members) 0)
      (db/delete-task-responsibles! {:task_id id
                                     :responsibles deleted-members}))
    (when (> (count new-tags) 0)
      (db/set-task-tags! {:tags (map (fn [t] [id t]) new-tags)}))
    (when (> (count deleted-tags) 0)
      (db/delete-task-tags! {:task_id id
                             :tags deleted-tags}))
    (= update-result 1)))


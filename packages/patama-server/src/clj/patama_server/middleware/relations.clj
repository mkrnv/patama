(ns patama-server.middleware.relations
  (:require 
    [patama-server.services.tags :as tags]))

(def too-many-args
  {:status 400
   :body {:success false
          :reason "too many parameters were given, need 2"}})

(def too-few-args
  {:status 400
   :body {:success false
          :reason "too few parameter was given, need 2"}})

(def wrong-ref
  {:status 400
   :body {:success false
          :reason "wrong reference was given"}})

(def wrong-task
  {:status 400
   :body {:success false
          :reason "wrong task specified. 'task' must be integer & id of specific task in the system"} })

(def not-implemented
  {:status 500
   :body {:success false
          :reason "sorry, this relationship check isn't implemented yet. but we hope that's not for long!"} })

(defn prepare
  [body]
  {:status 200
   :body body})

(defn check-task-tags
  [task-id]
  (tags/check-task-tags task-id))

(defn check-relations
  [params]
  (if (>
       (count params)
       2)
    too-many-args
  (if (<
       (count params)
       2)
    too-few-args
  (let [relation (get params :relation)
        task (get params :task)
        tag (get params :tag)]
    (if (not (nil? relation))
      (if (= relation "tags")
        (if (not (nil? task))
          (prepare (check-task-tags (read-string task)))
          ;;wrong-ref
          (println task))
        wrong-ref)
      (if (= relation "tasks")
        not-implemented
      not-implemented))))))

(defn establish-relation
  [params]
  (if (>
       (count params)
       2)
    too-many-args
  (if (<
       (count params)
       2)
    too-few-args
  (let [task (get params :task)
        tag (get params :tag)]
    (if (number? task)
      (if (string? tag)
        (prepare (tags/tag-task tag task))
        wrong-ref)
      not-implemented)))))

(ns patama-server.routes.services
  (:require
    [reitit.ring.coercion :as coercion]
    [reitit.coercion.spec :as spec-coercion]
    [reitit.ring.middleware.parameters :as parameters]
    [reitit.ring.middleware.muuntaja :as muuntaja]
    [reitit.ring.middleware.multipart :as multipart]
    [ring.util.http-response :refer :all]
    [clojure.java.io :as io]
    
    [patama-server.db.core :as db]

    [patama-server.services.auth :as auth]
    [patama-server.services.boards :as boards]
    [patama-server.services.tasks :as tasks]
    [patama-server.services.tags :as tags]
    [patama-server.services.projects :as projects]

    [patama-server.middleware.formats :as formats]
    [patama-server.middleware.relations :as relations]))

(def forbidden-message {:status 403})

(defn control-access
  [request handler]
  (let [granted (auth/validate-user-session request)]
    (if granted
      (handler)
      forbidden-message)))

(defn service-routes []
  ["/"
   {:summary "root"
    :coercion spec-coercion/coercion
    :muuntaja formats/instance
    :middleware [
                 ;; query-params & form-params
                 parameters/parameters-middleware
                 ;; content-negotiation
                 muuntaja/format-negotiate-middleware
                 ;; encoding response body
                 muuntaja/format-response-middleware
                 ;; exception handling
                 coercion/coerce-exceptions-middleware
                 ;; decoding request body
                 muuntaja/format-request-middleware
                 ;; coercing response bodys
                 coercion/coerce-response-middleware
                 ;; coercing request parameters
                 coercion/coerce-request-middleware
                 ;; multipart
                 multipart/multipart-middleware
                 ]}
   
   ;; swagger documentation
   ["" {:no-doc true
        :swagger {:info {:title "my-api"
                         :description "https://cljdoc.org/d/metosin/reitit"}}}]

   ["auth"
    {}
    ["/user"
     {:get  {:summary "get user information"
             :handler (fn [req]
                        (let [user-information (auth/get-user-information req)]
                          (if (nil? user-information)
                            {:status 401
                             :body "Unauthorized"}
                            {:status 200
                             :body {:success true
                                    :user user-information}})))}}]
    ["/register"
     {:post {:summary "create new account"
             :handler (fn [req]
                        (let [params          (get req :body-params)
                              username        (get params :username)
                              password        (get params :password)
                              creation-result (auth/create-account username password)]
                          (if (= creation-result auth/account-created)
                            {:status 200
                             :body {:success true}}
                            {:status 400
                             :body {:success false
                                    :reason creation-result}})))}}]
    ["/login"
     {:post {:summary "create new user session and return auth hash"
             :handler (fn [req]
                        (let [params       (get req :body-params)
                              username     (get params :username)
                              password     (get params :password)
                              user-session (auth/create-user-session username password)]
                          (if (some? user-session)
                            {:status 200
                             :body {:success true
                                    :user_session user-session}}
                            {:status 400
                             :body {:success false
                                    :reason "invalid login or password"}})))}}]]
   ["admin"
    {}
    ["/userlist"
     {:get {:summary "get user list"
            :handler (fn [req]
                       (control-access req
                          (fn []
                            (let [is-admin  (:is_admin (auth/get-user-information req))
                                  user-list (when is-admin (auth/get-user-list))]
                              (if (some? user-list)
                                {:status 200
                                 :body {:success true
                                        :user_list user-list}}
                                {:success 403
                                 :body {:success false
                                        :reason "no admin rights found"}})))))}}]]

   ["project"
    {}
    [""
    {:post {:summary "create new project"
            :handler (fn [req]
                       (control-access req
                         (fn []
                           (let [params (get req :body-params)
                                 project-id (projects/create-project! params)]
                             (if (nil? project-id)
                               {:status 400
                                :body {:success false
                                       :reason "unknown"}}
                               {:status 200
                                :body {:success true
                                       :project_id project-id}})))))}

     :get {:summary "get project(s) related to given params"
           :handler (fn [req]
                      (let [user-information (auth/get-user-information req)]
                        (if (nil? user-information)
                          {:status 401
                           :body {:success false
                                  :reason "Unauthorized"}}
                          (let [is-admin (:is_admin user-information)
                                username (:username user-information)
                                
                                id-from-query (get (:params req) :id)
                                result        (if (some? id-from-query)
                                                (projects/get-by-id id-from-query username)
                                                (if is-admin
                                                 (projects/get-owned username)
                                                 (projects/get-participated username)))]
                            (if (= result projects/not-allowed-to-view-project)
                              {:status 403
                               :body {:success false
                                      :reason projects/not-allowed-to-view-project}}
                              (if (nil? result)
                                {:status 400
                                 :body {:success false
                                        :reason "unknown"}}
                                {:status 200
                                 :body {:success true
                                        :result result}}))))))}}]
    ["/participants"
       {:get {:summary "get project team"
              :handler (fn [req]
                         (control-access req
                            (fn []
                              (let [participants (projects/get-project-participants
                                                   (:params req))]
                                {:status 200
                                 :body {:success true
                                        :participants participants}}))))}
        :post {:summary "update project team"
               :handler (fn [req]
                          (control-access req
                            (fn []
                              (let [params (:body-params req)]
                                (projects/update-participants! params)
                                {:status 200
                                 :body {:success true} }))))}}]]
  
   ["status"
    {:get {:summary "get list of available task statuses"
           :handler (fn [req]
                      (control-access req 
                        (fn []
                          (let [board-id (:board_id (:params req))]
                            {:status 200
                             :body (db/get-statuses {:board_id (Integer/parseInt board-id)})}))))}
     :post {:summary "register new task status"
            :handler (fn [req]
                       (control-access req
                         (fn [] 
                           (let [params (get req :body-params)
                                 id (get params :id)
                                 color (get params :color "bg-black")
                                 label (get params :label id)]
                             (db/create-status! {:id id
                                                 :color color
                                                 :label label})
                             {:status 200
                              :body {:success true}}))))}}]

   ["tag"
    {:get   {:summary "get list of available tags"
             :handler (fn [req]
                        (let [params (get req :params)]
                          {:status 200
                           :body (tags/get-available-tags params)}))}
     :post  {:summary "create new tag"
             :handler (fn [req]
                        (let [params (get req :body-params)]
                          (if (tags/create-tag params)
                            {:status 200
                             :body {:success true}}
                            {:status 400
                             :body {:success false
                                    :reason "unknown"}})))}
     :put   {:summary "update tag information"
             :handler (fn [req]
                        (let [params (get req :body-params)]
                          (if (tags/update-tag params)
                            {:status 200
                             :body {:success true}}
                            {:status 400
                             :body {:success false ; TODO: more verbose errors
                                    :reason "unknown"}})))}
     :delete{:summary "detach tag from tasks and delete it from db"
             :handler (fn [req]
                        (let [params (get req :params)
                              id (get params :id)]
                          (db/detach-tag! {:tag_id id})
                          (if (=
                               (db/delete-tag! {:id id})
                               1)
                            {:status 200
                             :body {:success true}}
                            {:status 500
                             :body {:success false
                                    :reason "invalid id"}})))}}
    ]

   ["relation"
    {:get 
     {:summary "check established relations between specific entity & model"
      :handler (fn [req]
                  (let [params (get req :params)]
                    (relations/check-relations params)))}

     :post
     {:summary "establish a relation between two entities"
      :handler (fn [req]
                  (let [params (get req :body-params)]
                    (relations/establish-relation params)))}}]

   ["board"
    {:get
     {:summary "get board(s) according to given parameters"
      :handler (fn [req]
                 (control-access req
                   (fn []
                     (let [params           (get req :params)
                           all-boards       (empty? params)
                           resulting-boards (if all-boards
                                              (boards/get-boards)
                                              (boards/get-boards-by-params params))]
                       {:status 200
                        :body {:success true
                               :result resulting-boards}}))))}
     :put
     {:summary "update board"
      :handler (fn [req]
                 (control-access req
                    (fn []
                      (let [params (get req :body-params)
                            update-result (boards/update-board! params)]
                        {:status 200
                         :body {:success true}}))))}

     :post
     {:summary "create new board"
      :handler (fn [req]
                 (let [params           (get req :body-params)
                       creation-result  (boards/create-board! params)]
                   (if (int? creation-result)
                     {:status 200
                      :body {:success true
                             :id creation-result
                             :title (:title params)}}
                     {:status 400
                      :body {:success false
                             :reason creation-result}})))}
    
     :delete
     {:summary "drop existing board"
      :handler (fn [req]
                 (let [params (get req :params)
                       deletion-result (boards/delete-board! params)]
                   (if (= deletion-result boards/board-delete-success)
                     {:status 200
                      :body {:success true}}
                     {:status 400
                      :body {:success false
                             :reason deletion-result}})))}}]

   ["task"
    {:get
     {:summary "get task or tasks by sort of parameters"
      :handler (fn [req]
                 (let [params (get req :params)]
                  {:status 200
                   :body (tasks/get-tasks-by params)}))}

     :post
     {:summary "create a new task"
      :handler (fn [req]
                 (control-access req
                    (fn []
                     (let [user   (:username (auth/get-user-information req))
                           params (assoc (get req :body-params) :user user)
                           creation-result (tasks/create-task! params)]
                       (if (int? creation-result)
                         {:status 200
                          :body {:success true
                                 :id creation-result}}
                         {:status 400
                          :body {:success false
                                 :reason creation-result}})))))}

     :put
     {:summary "update task information"
      :handler (fn [req]
                 (let [params   (get req :body-params)
                       snapshot (tasks/get-task-by-id (:id params))
                       result   (tasks/update-task! params)
                       ;; all-updates-valid (every? true? (for [x result] (first (vals x))))
                       ]
                   (if result
                     {:status 200
                      :body {:success true
                             :fields result}}
                     {:status 400
                      :body {:success false
                             :fields result
                             :snapshot snapshot}})))}

     :delete  {:summary "detach task from any tags & remove it from db"
               :handler (fn [req]
                          (let [params (get req :params)
                                id (read-string (get params :id))]
                            (db/detach-task-from-responsibles! {:task_id id})
                            (db/detach-task-from-tags! {:task_id id})
                            (if (=
                                 (db/delete-task! {:id id})
                                 1)
                              {:status 200
                               :body {:success true}}
                              {:status 500
                               :body {:success false
                                      :reason "invalid id"}})))}}]

   ["/files"
    {:swagger {:tags ["files"]}}

    ["/upload"
     {:post {:summary "upload a file"
             :parameters {:multipart {:file multipart/temp-file-part}}
             :responses {200 {:body {:name string?, :size int?}}}
             :handler (fn [{{{:keys [file]} :multipart} :parameters}]
                        {:status 200
                         :body {:name (:filename file)
                                :size (:size file)}})}}]

    ["/download"
     {:get {:summary "downloads a file"
            :swagger {:produces ["image/png"]}
            :handler (fn [_]
                       {:status 200
                        :headers {"Content-Type" "image/png"}
                        :body (-> "public/img/warning_clojure.png"
                                  (io/resource)
                                  (io/input-stream))})}}]]])

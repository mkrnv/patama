(ns patama-server.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [patama-server.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[patama-server started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[patama-server has shut down successfully]=-"))
   :middleware wrap-dev})

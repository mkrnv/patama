(ns patama-server.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[patama-server started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[patama-server has shut down successfully]=-"))
   :middleware identity})

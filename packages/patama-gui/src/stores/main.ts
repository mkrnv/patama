import { defineStore } from 'pinia'
import { UserInformation } from '../lib/types'

export const useAuthStore = defineStore('auth', {
    state: () => {
        return {
            userInformation: {} as UserInformation,
            token: '' as string
        }
    },
    getters: {
        isAdmin(): boolean {
            return this.userInformation.is_admin
        },
        username(): string {
            return this.userInformation.username
        }
    },
    actions: {
        authorize(userInformation: UserInformation) {
            this.userInformation = userInformation
        },
        authenticate(token: string) {
            this.token = token
        }
    }
})

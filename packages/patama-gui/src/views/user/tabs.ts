export default (isAdmin = false) => isAdmin
    ?
        [
            { tab: '/user', label: 'Профиль'},
            { tab: '/user/list', label: 'Список пользователей 👑'}
        ]
    :
        [
            { tab: '/user', label: 'Профиль'}
        ]

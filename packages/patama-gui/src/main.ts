import { createApp } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'
import { createPinia } from 'pinia'

import App from './App.vue'

import Markdown from 'vue3-markdown-it'
import ColorPicker from 'vue3-colorpicker'

import 'vue3-colorpicker/style.css'
import './index.css'

import Home from '@/views/home/Index.vue'

import User from '@/views/user/Index.vue'
import UserList from '@/views/user/List.vue'

import Project from '@/views/projects/Index.vue'

import Board from '@/views/boards/Index.vue'

const routes = [
    { path: '/', component: Home },

    { path: '/user', component: User },
    { path: '/user/list', component: UserList },

    { path: '/projects/:id', component: Project },

    { path: '/boards/:id', component: Board }
]

const router = createRouter({
    routes, history: createWebHistory()
})

const pinia = createPinia()

createApp(App)
    .use(Markdown)
    .use(ColorPicker)
    .use(router)
    .use(pinia)
    .mount('#app')

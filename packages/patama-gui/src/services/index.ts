import { AuthService } from "./auth";
import BoardService from "./boards";
import ProjectService from "./projects";
import { TaskService } from "./tasks";

export const services = {
    auth: new AuthService(),
    boards: new BoardService(),
    projects: new ProjectService(),
    tasks: new TaskService()
}

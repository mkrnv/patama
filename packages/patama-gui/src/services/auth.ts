import { useRouter } from 'vue-router'
import { useCookies } from "vue3-cookies";

import { UserInformation } from "../lib/types";

import { useAuthStore } from "../stores/main";

export class AuthService {
    constructor() {

    }

    async getAuth() : Promise<string> {
        return new Promise<string>(async (resolve, reject) => {
            const { cookies } = useCookies()
            if (!cookies.isKey('auth')) resolve('')

            const auth = `${cookies.get('auth')}`
            const response = await fetch(import.meta.env?.VITE_API + '/auth/user', {
                headers: { Authorization: auth }
            })
            if (response.status !== 200) reject()

            const user = (await response.json()).user as UserInformation
            console.log(user)
            const store = useAuthStore()
            store.authorize(user)
            store.authenticate(auth)
            resolve(auth)
        })
    }
    
    getQueryAuth() : void {        
        const url = new URL(window.location.href)
        const searchParams = url.searchParams

        const auth = searchParams.get('auth')

        if (!auth) return

        const { cookies } = useCookies()
        cookies.set('auth', auth)

        window.location.href = '/'
    }

    logOut() : void {
        const { cookies } = useCookies()
        cookies.remove('auth')

        window.location = import.meta.env?.VITE_LOGIN
    }
}

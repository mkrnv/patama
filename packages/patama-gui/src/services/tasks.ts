import { Task, TaskModel, TaskTag } from "../lib/types"

import { useAuthStore } from "@/stores/main"

import { services } from "."
import { AuthService } from "./auth"

export class TaskService {
    private _authService: AuthService

    constructor() {
    }
    
    getByStatuses(boardId: number): Promise<Task[]> {
        return new Promise<Task[]>(async (resolve, reject) => {
            const response = await fetch(import.meta.env?.VITE_API + `/task?board_id=${boardId}`)
            if (response.status == 200) {
                const tasks = await response.json()
                resolve(tasks)
            }
            reject()
        })
    }

    async createTask(task: TaskModel, boardId: number): Promise<string> {
        const authStore = useAuthStore()
        if (!authStore.token) return Promise.reject()

        return new Promise(async (resolve, reject) => {
            const response = await fetch(import.meta.env?.VITE_API + '/task', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json', Authorization: authStore.token },
                body: JSON.stringify({
                    title: task.title,
                    description: task.description,
                    until: (new Date(task.date)).toISOString().split('T').join(' ').split('Z')[0].split('.')[0],
                    status: task.status,
                    tags: task.tags,
                    board_id: boardId
                })
            })
            if (response.status == 200)
                return resolve('OK')
            
            if (response.status !== 500) {
                const errorJson = await response.json()
                resolve(`Ошибка: ${errorJson?.data ?? "неизвестная"}`)
            }

            reject()
        })
    }

    async updateTask(updatedTask: TaskModel, originalTask: TaskModel): Promise<string> {
        const simpleValues = ['title', 'description', 'date'] as const
        type SimpleTaskValue = typeof simpleValues[number]
        return new Promise(async (resolve, reject) => {
            const payload = {} as Record<string, any>
            Object.entries(updatedTask).forEach(entry => {
                const [key, value]: [any, any] = entry
                if (simpleValues.includes(key) && originalTask[key as SimpleTaskValue] !== value)
                    switch (key) {
                        case 'date':
                            payload[key as SimpleTaskValue] = new Date(value).toISOString().replace('T', ' ').replace('Z', '')
                            break;
                        default:
                            payload[key as SimpleTaskValue] = value
                            break;
                    }
            })

            payload.id = updatedTask.id
            payload.responsibles = updatedTask.responsibles
            payload.tags = updatedTask.tags

            console.log(payload)

            const body = JSON.stringify(payload)
            
            const response = await fetch(import.meta.env?.VITE_API + `/task`, {
                method: 'PUT',
                body,
                headers: { 'Content-Type': 'application/json' }
            })
            if (response.status == 200)
                resolve(await response.json())
            else
                reject()
        })
    }

    async deleteTask(id: number): Promise<void> {
        return new Promise(async (resolve, reject) => {
            const response = await fetch(import.meta.env?.VITE_API + `/task?id=${id}`, {
                method: 'DELETE'
            })
            if (response.status == 200)
                resolve()
            else 
                reject()
        })
    }
}

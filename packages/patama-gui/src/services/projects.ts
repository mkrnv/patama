import { Project } from "../lib/types"
import { useAuthStore } from "../stores/main"

export default class ProjectService {
    constructor() {

    }

    async createProject(title: string) : Promise<number> {
        return new Promise<number>(async (resolve, reject) => {
            const authStore = useAuthStore()
            if (!authStore.token) return
            
            const response = await fetch(import.meta.env?.VITE_API + '/project', {
                method: 'POST',
                body: JSON.stringify({ title, owner: authStore.username }),
                headers: { Authorization: authStore.token, 'Content-Type': 'application/json' }
            })

            if (response.status !== 200) reject()
            resolve((await response.json()).project_id)
        })
    }

    async getProjects() : Promise<Project[]> {
        return new Promise<Project[]>(async (resolve, reject) => {
            const authStore = useAuthStore()
            if (!authStore.token) return
            
            const response = await fetch(import.meta.env?.VITE_API + '/project', {
                headers: { Authorization: authStore.token }
            })

            if (response.status !== 200) reject()
            resolve((await response.json()).result)
        })
    }
    
    async getProjectById(projectId: number) : Promise<Project> {
        return new Promise<Project>(async (resolve, reject) => {
            const authStore = useAuthStore()
            if (!authStore.token) return

            const response = await fetch(import.meta.env?.VITE_API + `/project?id=${projectId}`, {
                headers: { Authorization: authStore.token }
            })

            if (response.status !== 200) reject(response.status)
            else resolve((await response.json()).result)
        })
    }

    async getProjectParticipants(projectId: number) : Promise<string[]> {
        const authStore = useAuthStore()
        if (!authStore.token) return Promise.reject()
        
        return new Promise<string[]>(async (resolve, reject) => {
            const response = await fetch(import.meta.env?.VITE_API + `/project/participants?project_id=${projectId}`, {
                headers: { Authorization: authStore.token }
            })

            if (response.status !== 200) reject(response.status)
            else resolve((await response.json()).participants)
        })
    }

    async updateParticipants(projectId: number, participants: string[]) : Promise<void> {
        const authStore = useAuthStore()
        if (!authStore.token) return Promise.reject()
        
        return new Promise<void>(async (resolve, reject) => {
            const response = await fetch(import.meta.env?.VITE_API + `/project/participants`, {
                method: 'POST',
                headers: { Authorization: authStore.token, 'Content-Type': 'application/json' },
                body: JSON.stringify({ project_id: Number(projectId), participants })
            })

            if (response.status !== 200) reject(response.status)
            else resolve()
        })

    }
}

import { Board, TaskStatus } from '../lib/types'
import { useAuthStore } from '../stores/main'

export default class BoardService {
    constructor() {

    }

    getCurrentBoard(boards: Board[]): Board {
        const currentBoardId = Number(window.localStorage.getItem('activeBoard'))
        return boards.filter(b =>
                b.id == (!currentBoardId
                    ? boards[0].id
                    : currentBoardId))[0]
    }

    async getBoardById(id: number) : Promise<Board> {
        const authStore = useAuthStore()
        if (!authStore.token) return Promise.reject()

        return new Promise<Board>(async (resolve, reject) => {
            const response = await fetch(import.meta.env?.VITE_API + `/board?id=${id}`, {
                headers: { Authorization: authStore.token }
            })
            if (response.status !== 200) reject()

            resolve((await response.json()).result)
        })
    }

    async getBoardsByProjectId(projectId: number) : Promise<Board[]> {
        const authStore = useAuthStore()
        if (!authStore.token) return Promise.reject()

        return new Promise<Board[]>(async (resolve, reject) => {
            const response = await fetch(import.meta.env?.VITE_API + `/board?project_id=${projectId}`, {
                headers: { Authorization : authStore.token}
            })

            if (response.status !== 200) reject()
            resolve((await response.json()).result)
        })
    }

    async updateBoard(board: Board) : Promise<void> {
        const authStore = useAuthStore()
        if (!authStore.token) return Promise.reject()

        board.columns.forEach(c => {
            delete c.tasks
        })

        return new Promise(async (resolve, reject) => {
            const response = await fetch(import.meta.env?.VITE_API + '/board', {
                method: 'PUT',
                body: JSON.stringify(board),
                headers: { Authorization: authStore.token, 'Content-Type': 'application/json' }
            })

            if (response.status === 200) resolve()
            else reject()
        })
    }

    async getBoards() : Promise<Board[] | undefined> {
        const boardRequest = await fetch(import.meta.env?.VITE_API + "/board")
        if (boardRequest.status !== 200) return

        const json = await boardRequest.json()
        if (!json?.success) return

        const boards = json?.boards
        if (!boards) return

        return boards as Board[]
    }

    async getStatuses(boardId: number) : Promise<TaskStatus[]> {
        const authStore = useAuthStore()
        if (!authStore.token) return Promise.reject()

        return new Promise<TaskStatus[]>(async (resolve, reject) => {
            const response = await fetch(import.meta.env?.VITE_API + `/status?board_id=${boardId}`, {
                headers: { Authorization: authStore.token }
            })
            if (response.status !== 200) reject()

            resolve(await response.json()) 
        })

    }
}

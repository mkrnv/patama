export type Task = {
    id: number,
    title: string,
    description?: string,
    until?: Date,
    timestamp?: Date,
    status: string
}

export type TaskModel = {
    id: number
    title: string
    description: string
    date: string
    status: string
    responsibles: string[]
    tags: TaskTag[]
}

export type TaskStatus = {
    id: string,
    color: string,
    label: string,
    tasks?: Task[]
}

export type TaskTag = {
    id: string,
    color: string,
    label: string
}

export type Board = {
    id: number,
    title: string
    columns: TaskStatus[]
    tags: TaskTag[]
}

export type Project = {
    id: number,
    title: string
}

export type UserInformation = {
    username: string
    full_name: string
    occupation: string
    email: string,
    is_admin: boolean
}

import { services } from '../services'
import { TaskStatus, TaskTag, Task } from './types'


export const fetchStatuses = () => {
    const auth = services.auth.getAuth()

    return new Promise<TaskStatus[]>(async (resolve, reject) => {
        const response = await fetch(import.meta.env?.VITE_API + '/status', {
            headers: {
                Authorization: auth
            }
        })
        if (response.status == 200) {
            const statuses = await response.json()
            resolve(statuses)
        }
        reject()
    })
}

export const fetchTasks = (status: string) => {
    return new Promise<Task[]>(async (resolve, reject) => {
        const response = await fetch(import.meta.env?.VITE_API + `/task?status=${status}`)
        if (response.status == 200) {
            const tasks = await response.json()
            resolve(tasks)
        }
        reject()
    })
}

export const fetchTaskTags = (task: number) => {
    return new Promise<TaskTag[]>(async (resolve, reject) => {
        const response = await fetch(import.meta.env?.VITE_API + `/relation?task=${task}&relation=tags`)
        if (response.status == 200) {
            const tags = await response.json()
            resolve(tags)
        }
        reject()

    })
}

export const tagTask = (task: number, tag: string) => {
    return new Promise<boolean>(async (resolve, reject) => {
        const response = await fetch(import.meta.env?.VITE_API +  '/relation', {
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                task, tag
            }),
            method: 'POST'
        })
        if (response.status == 200) {
            resolve(true)
        }
        else {
            reject()
        }
    })
}
